function ls --wraps='exa --icons --group-directories-first -a' --description 'alias ls=exa --icons --group-directories-first -a'
  exa --icons --group-directories-first -a $argv
        
end
