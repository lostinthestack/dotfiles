set fish_cursor_default block
set fish_cursor_insert line
set fish_cursor_replace_one underscore
set fish_cursor_visual block

export PAGER='bat'

if status is-interactive
    # Commands to run in interactive sessions can go here
end

