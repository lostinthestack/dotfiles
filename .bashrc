#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# alias ls='ls --color=auto'
# alias grep='grep --color=auto'
# PS1='[\u@\h \W]\$ '

shopt -s autocd

alias ls='exa --icons --group-directories-first -a'

export PATH=$PATH:$HOME/.cargo/bin:$PATH
export PAGER=bat
export EDITOR=nvim
export VISUAL=nvim
export TERMINAL=kitty

fetcho
